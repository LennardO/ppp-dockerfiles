#!/usr/bin/env bash
set -e

docker build . -f Cuda -t cuda

docker run --rm 																\
					 --cap-add sys_ptrace 								\
					 -p127.0.0.1:2221:22 									\
					 -e DISPLAY=$DISPLAY 									\
					 -v /tmp/.X11-unix:/tmp/.X11-unix 		\
					 --device /dev/dri 										\
					 --privileged 												\
					 --name ci_cuda 											\
					 cuda 																\
					 nvidia-smi

docker run --rm 																\
					 --cap-add sys_ptrace 								\
					 -p127.0.0.1:2221:22 									\
					 -e DISPLAY=$DISPLAY 									\
					 -v /tmp/.X11-unix:/tmp/.X11-unix 		\
					 --device /dev/dri 										\
					 --privileged 												\
					 --name ci_cuda 											\
					 -v $(pwd)/tests:/tests 							\
					 cuda 																\
					 bash -c "mkdir -p /tests/cuda/ci-build 		&&\
						 			  cd /tests/cuda/ci-build 					&&\
							 		  cmake .. 												 	&&\
									  make -j $(nproc)								 	&&\
									  ./cuda-test"



docker build . -f Raja -t raja

docker run --rm 																\
					 --cap-add sys_ptrace 								\
					 -p127.0.0.1:2221:22 									\
					 -e DISPLAY=$DISPLAY 									\
					 -v /tmp/.X11-unix:/tmp/.X11-unix 		\
					 --device /dev/dri 										\
					 --privileged 												\
					 --name ci_raja 											\
					 -v $(pwd)/tests:/tests 							\
					 raja																	\
					 bash -c "mkdir -p /tests/raja/ci-build 		&&\
						 			  cd /tests/raja/ci-build 					&&\
							 		  cmake -DENABLE_OPENMP=Off						\
													-DENABLE_CUDA=On .. 				&&\
									  make -j $(nproc)								 	&&\
									  ./bin/raja-test"

docker run --rm 																\
					 --cap-add sys_ptrace 								\
					 -p127.0.0.1:2221:22 									\
					 -e DISPLAY=$DISPLAY 									\
					 -v /tmp/.X11-unix:/tmp/.X11-unix 		\
					 --device /dev/dri 										\
					 --privileged 												\
					 --name ci_raja 											\
					 -v $(pwd)/tests:/tests 							\
					 raja																	\
					 bash -c "mkdir -p /tests/raja/ci-build 		&&\
						 			  cd /tests/raja/ci-build 					&&\
							 		  cmake -DENABLE_OPENMP=On						\
													-DENABLE_CUDA=Off .. 				&&\
									  make -j $(nproc)								 	&&\
									  ./bin/raja-test"

docker build . -f Kokkos -t kokkos

docker run --rm 																\
					 --cap-add sys_ptrace 								\
					 -p127.0.0.1:2221:22 									\
					 -e DISPLAY=$DISPLAY 									\
					 -v /tmp/.X11-unix:/tmp/.X11-unix 		\
					 --device /dev/dri 										\
					 --privileged 												\
					 --name ci_kokkos											\
					 -v $(pwd)/tests:/tests 							\
					 kokkos																\
					 bash -c "mkdir -p /tests/kokkos/ci-build_cuda 	&&\
						 			  cd /tests/kokkos/ci-build_cuda 				&&\
							 		  cmake -DUSE_CUDA=ON ..								&&\
									  make -j $(nproc)			 								&&\
									  time ./kokkos-test"
# docker run --rm --cap-add sys_ptrace -p127.0.0.1:2221:22 -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --device /dev/dri --privileged --name ci_kokkos -v $(pwd)/tests:/tests kokkos bash -c "mkdir -p /tests/kokkos/ci-build_cuda && cd /tests/kokkos/ci-build_cuda && cmake -DUSE_CUDA=ON .. && make -j $(nproc) && time ./kokkos-test"
									  
docker run --rm 																\
					 --cap-add sys_ptrace 								\
					 -p127.0.0.1:2221:22 									\
					 -e DISPLAY=$DISPLAY 									\
					 -v /tmp/.X11-unix:/tmp/.X11-unix 		\
					 --device /dev/dri 										\
					 --privileged 												\
					 --name ci_kokkos											\
					 -v $(pwd)/tests:/tests 							\
					 kokkos																\
					 bash -c "mkdir -p /tests/kokkos/ci-build_omp 	&&\
						 			  cd /tests/kokkos/ci-build_omp 				&&\
							 		  cmake .. 															&&\
									  make -j $(nproc)			 								&&\
									  time ./kokkos-test"


docker build . -f HipSYCL -t hipsycl

docker run --rm 																\
					 --cap-add sys_ptrace 								\
					 -p127.0.0.1:2221:22 									\
					 -e DISPLAY=$DISPLAY 									\
					 -v /tmp/.X11-unix:/tmp/.X11-unix 		\
					 --device /dev/dri 										\
					 --privileged 												\
					 --name ci_hipsycl										\
					 -v $(pwd)/tests:/tests 							\
					 hipsycl															\
					 bash -c "mkdir -p /tests/hipsycl/ci-build 		&&\
						 			  cd /tests/hipsycl/ci-build 					&&\
							 		  cmake -D HIPSYCL_PLATFORM=cpu	..		&&\
									  make -j $(nproc)			 							&&\
									  time ./hipsycl-test"
# docker run --rm --cap-add sys_ptrace -p127.0.0.1:2221:22 -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --device /dev/dri --privileged --name ci_hipsycl -v $(pwd)/tests:/tests hipsycl bash -c "mkdir -p /tests/hipsycl/ci-build && cd /tests/hipsycl/ci-build && cmake -D HIPSYCL_PLATFORM=cpu .. && make -j $(nproc) && time ./hipsycl-test"
									  

docker build . -f NVIDIA-HPC-SDK -t hpc-sdk

docker run --rm 																\
					 --cap-add sys_ptrace 								\
					 -p127.0.0.1:2221:22 									\
					 -e DISPLAY=$DISPLAY 									\
					 -v /tmp/.X11-unix:/tmp/.X11-unix 		\
					 --device /dev/dri 										\
					 --privileged 												\
					 --name ci_openmp											\
					 -v $(pwd)/tests:/tests 							\
					 hpc-sdk															\
					 bash -c "mkdir -p /tests/openmp/ci-build 		&&\
						 			  cd /tests/openmp/ci-build 					&&\
							 		  cmake	..														&&\
									  make -j $(nproc)			 							&&\
									  time ./openmp-test"


docker build . -f OpenCL -t opencl

docker run --rm 																\
					 --cap-add sys_ptrace 								\
					 -p127.0.0.1:2221:22 									\
					 -e DISPLAY=$DISPLAY 									\
					 -v /tmp/.X11-unix:/tmp/.X11-unix 		\
					 --device /dev/dri 										\
					 --privileged 												\
					 --name ci_opencl											\
					 -v $(pwd)/tests:/tests 							\
					 opencl																\
					 bash -c "mkdir -p /tests/opencl/ci-build 		&&\
						 			  cd /tests/opencl/ci-build 					&&\
							 		  cmake	..														&&\
									  make -j $(nproc)			 							&&\
									  time ./opencl-test"


# docker run --rm --cap-add sys_ptrace -p127.0.0.1:2221:22 -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix --device /dev/dri --privileged --name ci_openmp -v $(pwd)/tests:/tests hpc-sdk bash -c "mkdir -p /tests/openmp/ci-build && cd /tests/openmp/ci-build && cmake .. && make -j $(nproc) && time ./openmp-test"
