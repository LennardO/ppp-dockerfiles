__kernel void saxpyKernel(float  a,
													__global float* x,
													__global float* y)
{
	auto i = get_global_id(0);
	y[i] = a*x[i] + y[i];
}
