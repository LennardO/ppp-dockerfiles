
#include "saxpy.h"
#include <iostream>
#include <CL/sycl.hpp>

void saxpy(float a, const std::vector<float>& x, std::vector<float>& y) {
	
	std::cout << "known platforms:\n";
	for(auto const& platform : cl::sycl::platform::get_platforms())
		std::cout << "  " << platform.get_info<cl::sycl::info::platform::name>() << '\n';
	std::cout << "known devices:\n";
	for(auto const& device : cl::sycl::device::get_devices()) {
		std::string typeStr = device.is_gpu() ? "gpu" : "no gpu";
		std::cout << "  " << typeStr << '\n';
	}
	
	cl::sycl::queue q;
	cl::sycl::range<1> work_items{x.size()};
	
	{
		cl::sycl::buffer<float> x_buff(x.data(), x.size());
    cl::sycl::buffer<float> y_buff(y.data(), y.size());
    
    q.submit([&](cl::sycl::handler& cgh){
      auto x_access = x_buff.get_access<cl::sycl::access::mode::read>(cgh);
      auto y_access = y_buff.get_access<cl::sycl::access::mode::read_write>(cgh);

      cgh.parallel_for<class saxpy>(work_items,
                                    [=] (cl::sycl::id<1> tid) {
        y_access[tid] = a*x_access[tid] + y_access[tid];
      });
    });
	}
	
}
