
#include "saxpy.h"
#include <iostream>
#include <Kokkos_Core.hpp>

#if defined(USE_CUDA)
	using ExecSpace = Kokkos::Cuda;
	using MemSpace = Kokkos::CudaSpace;
#else
	using ExecSpace = Kokkos::OpenMP;
	using MemSpace = Kokkos::HostSpace;
#endif

void saxpy(float a, const std::vector<float>& x, std::vector<float>& y) {
	Kokkos::initialize();
	{
		int N = x.size();
		
		ExecSpace ex;
		std::cout << "saxpy with N=" << N << " using Execution Space: " << ex.name() << '\n';
		std::cout << "configuration of " << ex.name() << ":\n";
		ex.print_configuration(std::cout, true);
		
		Kokkos::View<float*, MemSpace> x_view("x_view", N);
		auto x_hostView = Kokkos::create_mirror_view(x_view);
		std::memcpy(&x_hostView(0), &x[0], sizeof(float)*N);
		Kokkos::deep_copy(x_view, x_hostView);
		
		Kokkos::View<float*, MemSpace> y_view("y_view", N);
		auto y_hostView = Kokkos::create_mirror_view(y_view);
		std::memcpy(&y_hostView(0), &y[0], sizeof(float)*N);
		Kokkos::deep_copy(y_view, y_hostView);
		
		
		Kokkos::parallel_for("aX+Y", Kokkos::RangePolicy<ExecSpace>(ex, 0,N), KOKKOS_LAMBDA (const int& i) {
			y_view[i] = a*x_view[i] + y_view[i];
		});

		Kokkos::deep_copy(y_hostView, y_view);
		std::memcpy(&y[0], &y_hostView(0), sizeof(float)*N);
  }
  Kokkos::finalize();
}
