
#include "saxpy.h"
#include <iostream>


void saxpy(float a, const std::vector<float>& x, std::vector<float>& y) {
	
	int N = x.size();
	const float* x_ptr = x.data();
	float* y_ptr = y.data();
	
	#pragma omp target enter data map(to: x_ptr[0:N]) map(to: y_ptr[0:N])
	
	#pragma omp target
	#pragma omp parallel for
	for(int i=0; i<N; i++)
		y_ptr[i] = a*x_ptr[i] + y_ptr[i];
	
	#pragma omp target exit data map(delete: x_ptr[0,N]) map(from: y_ptr[0:N])
}
